**Experis Academy, Norway**

**Authors:**
* **Ørjan Storås**

# Experis_Week2_Task11

## Task 11: Payment system

Program for creating a payment-item; card or cash; fill it with currency and then pay/withdraw a specified sum from that payment item. 
