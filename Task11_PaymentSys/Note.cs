﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSys
{
    class Note
    {
        private int value;

        public Note(int value)
        {
            this.value = value;
        }

        public int Value { get => value; set => this.value = value; }
    }
}
