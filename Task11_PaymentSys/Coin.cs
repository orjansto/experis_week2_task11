﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSys
{
    /// <summary>
    /// Class for creating and handeling coins of varying values.
    /// </summary>
    class Coin
    {
        private int value;

        public Coin(int value)
        {
            this.value = value;
        }

        public int Value { get => value; set => this.value = value; }
    }
}
