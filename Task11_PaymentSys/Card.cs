﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSys
{
    class Card
    {

        private Balance cardBalance;

        public Card()
        {
            cardBalance = new Balance(0);
        }
        public Card(Balance cardBalance)
        {
            this.cardBalance = cardBalance;
        }
        public Card(int amount)
        {
            cardBalance = new Balance(amount);
        }

        internal Balance CardBalance { get => cardBalance; set => cardBalance = value; }
    }
}
