﻿using System;

namespace Task11_PaymentSys
{
    class Program
    {
        static void Payment(IPayment payment)
        {
            string input;
            do
            {
                payment.Payment();
                Console.WriteLine("Would you like to conduct another transaction? y/n");
                do
                {
                    input = Console.ReadLine().ToLower();
                } while (!(input=="y"||input=="n"));
                

            } while (input=="y");
            
        }
        static IPayment createPaymentOption()
        {
            bool success = true;
            int choice = 1;
            int amount = 0;
            Console.WriteLine("You will now create a payment method");
            Console.WriteLine("You have the options of:");
            Console.WriteLine("[1] Credit Card");
            Console.WriteLine("[2] Debit Card");
            Console.WriteLine("[3] Cash");
            do
            {
                if (!success || choice > 3 || choice < 1)
                {
                    Console.WriteLine("Please enter an valid option. (1,2 or 3)");
                }
                success = int.TryParse(Console.ReadLine(), out choice);
            } while (!success||choice>3||choice<1);

            switch (choice)
            {
                case 1:
                    Console.WriteLine("Creating a Credit Card");
                    CreditCard creditCard = new CreditCard();

                    Console.WriteLine("What is your desired credit limit?");
                    do
                    {
                        if (!success)
                        {
                            Console.WriteLine("Please enter a real number:");
                        }
                        success = int.TryParse(Console.ReadLine(), out amount);
                    } while (!success);
                    creditCard.CardBalance.Deposit(amount);
                    return creditCard;
                case 2:
                    Console.WriteLine("Creating a Debit Card");
                    DebitCard debitCard = new DebitCard();

                    Console.WriteLine("How much money do your card contain?");
                    do
                    {
                        if (!success)
                        {
                            Console.WriteLine("Please enter a real number:");
                        }
                        success = int.TryParse(Console.ReadLine(), out amount);
                    } while (!success);
                    debitCard.CardBalance.Deposit(amount);
                    return debitCard;
                case 3:
                    Console.WriteLine("Creating a pouch with coins");
                    Cash coinPouch = new Cash();
                    Console.WriteLine();
                    coinPouch.addCoinsConsole();
                    Console.WriteLine();
                    coinPouch.addNotesConsole();
                    coinPouch.printBalance();

                    return coinPouch;
                default:
                    Console.WriteLine("Payment method creation failed!");
                    return null;
            }

            
        }
        static void Main(string[] args)
        {
            string input;
            Console.WriteLine("---Task11---");
            Console.WriteLine("------------");
            Console.WriteLine("Push enter to start:");
            Console.ReadLine();
            do
            {
                input = "FUBAR";
                try
                {
                    Console.WriteLine();
                    Payment(createPaymentOption());

                    

                }
                catch(Exception ex)
                {
                    Console.WriteLine("Error occurred!");
                    Console.WriteLine(ex.Message);
                }
                Console.WriteLine("Would you like to rerun program? y/n");
                do
                {
                    if(input != "FUBAR")
                    {
                        Console.WriteLine("Please enter valid input [y] or [n]");
                    }
                    input = Console.ReadLine().ToLower();
                } while (!(input == "y" || input == "n"));

            } while (input == "y");
        }
    }
}
