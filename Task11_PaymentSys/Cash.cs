﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSys
{
    class Cash : IPayment
    {
        public void Payment()
        {
            bool paymAccept = false;
            int paymen;
            Console.WriteLine("You will now pay with Cash");
            do
            {
                Console.WriteLine("Please enter amount:");
                paymAccept = int.TryParse(Console.ReadLine(), out paymen);
                if (paymAccept)
                {
                    Console.WriteLine($"You try to pay {paymen} in cash.");
                    PayAmount(paymen);
                    /*//Console.WriteLine($"You try to find {paymen} coins from your money-pouch");
                    //if (!CardBalance.Withdrawal(paymen))
                    if(cashBalance.Reserve<paymen)
                    {
                        Console.WriteLine("With dissapointment you realise your pouch do not contain enough cash.");
                        Console.WriteLine($"You only find {CardBalance.Reserve} in a combination of {sumOfCoins} in coins and {sumOfNotes} in notes..");
                    }
                    else
                    {
                        Console.WriteLine($"You hand over {paymen} coins.");
                        Console.WriteLine($"You have {CardBalance.Reserve} coins left.");
                    }*/
                }

            } while (!paymAccept);
        }

        private Balance cashBalance;
        private List<Note> notes;
        private List<Coin> coins;
        private int sumOfNotes;
        private int sumOfCoins;

        public Cash()
        {
            notes = new List<Note>();
            coins = new List<Coin>();
            updateBalance();
        }
        public Cash(List<Coin> coinList)
        {
            coins = coinList;
            notes = new List<Note>();
            updateBalance();
        }
        public Cash(List<Note> noteList)
        {
            notes = noteList;
            coins = new List<Coin>();
            updateBalance();
        }
        public Cash(List<Coin> coinList, List<Note> noteList)
        {
            coins = coinList;
            notes = noteList;
            updateBalance();  
        }
        

        internal virtual Balance CardBalance { get => cashBalance; }

        public void addCoins(int quant, int value)
        {
            for (int i = 0; i < quant; i++)
            {
                coins.Add(new Coin(value));                
            }
            updateBalance();
        }
        public void addNotes(int quant, int value)
        {
            for (int i = 0; i < quant; i++)
            {
                notes.Add(new Note(value));
            }
            updateBalance();
        }

        public void addCoinsConsole()
        {
            string input;
            do
            {
                int quantity;
                int value;
                int limit = 100;
                bool success = false;
                Console.WriteLine("Adding Coins of same value:");

                Console.WriteLine("How many coins will you add?");
                do
                {
                    Console.WriteLine($"Add a natural number less than {limit}");
                    success = int.TryParse(Console.ReadLine(), out quantity);
                } while (!success && (quantity < limit) && quantity != 0);
                success = false;

                Console.WriteLine("What is the coins value?");
                do
                {
                    Console.WriteLine($"Add a natural number less than {limit}");
                    success = int.TryParse(Console.ReadLine(), out value);
                } while (!success && (value < limit) && value != 0);
                success = false;
                addCoins(quantity, value);

                Console.WriteLine("Would you like to add more coins? y/n");
                do
                {
                    input = Console.ReadLine().ToLower();
                } while (!(input == "y" || input == "n"));
            } while (input == "y");




            

        }
        public void addNotesConsole()
        {
            string input;
            do
            {
                int quantity;
                int value;
                int limit = 100;
                bool success = false;
                Console.WriteLine("Adding notes of same value:");

                Console.WriteLine("How many notes will you add?");
                do
                {
                    Console.WriteLine($"Add a natural number less than {limit}");
                    success = int.TryParse(Console.ReadLine(), out quantity);
                } while (!success && (quantity < limit) && quantity != 0);
                success = false;

                Console.WriteLine("What is the notes value?");
                do
                {
                    Console.WriteLine($"Add a natural number less than {1000}");
                    success = int.TryParse(Console.ReadLine(), out value);
                } while (!success && (value < 1000) && value != 0);
                success = false;
                addNotes(quantity, value);

                Console.WriteLine("Would you like to add more notes? y/n");
                do
                {
                    input = Console.ReadLine().ToLower();
                } while (!(input == "y" || input == "n"));
            } while (input == "y");
        }

        public bool HaveSufficientFunds(int value)
        {
            if (value <= cashBalance.Reserve)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void printBalance()
        {
            Console.WriteLine($"You have {cashBalance.Reserve} cash;");
            Console.WriteLine($"Whereas {coins.Count} coins totaling {sumOfCoins},");
            Console.WriteLine($"And {notes.Count} notes totaling {sumOfNotes}.");
        }

        private void updateBalance()
        {
            cashBalance = new Balance(0);
            sumOfCoins = 0;
            sumOfNotes = 0;
            foreach (Coin c in coins)
            {
                cashBalance.Reserve += c.Value;
                sumOfCoins += c.Value;
            }
            foreach (Note n in notes)
            {
                cashBalance.Reserve += n.Value;
                sumOfNotes += n.Value;
            }
            coins.Sort((y, x) => x.Value.CompareTo(y.Value));
            notes.Sort((y, x) => x.Value.CompareTo(y.Value));

        }

        public bool PayAmount(int amount)
        {
            int numberOfNotesPaid = 0;
            int numberOfCoinsPaid = 0;
            Note restNote = new Note(0);
            Coin restCoin = new Coin(0);
            if (HaveSufficientFunds(amount))
            {
                List<Note> dummyNotes = new List<Note>(notes);

                Console.WriteLine("You will pay with:");
                foreach (Note n in dummyNotes)
                {
                    if (n.Value <= amount)
                    {
                        Console.WriteLine($"A note of value: {n.Value}");
                        numberOfNotesPaid++;
                        amount -= n.Value;
                        notes.Remove(n);
                    }
                    else
                    {
                        restNote = n;
                    }
                }
                List<Coin> dummyCoins = new List<Coin>(coins);
                foreach (Coin c in dummyCoins)
                {
                    if (c.Value <= amount)
                    {
                        Console.WriteLine($"A coin of value: {c.Value}");
                        numberOfCoinsPaid++;
                        amount -= c.Value;
                        coins.Remove(c);
                    }
                    else
                    {
                        restCoin = c;
                    }
                }
                if (amount == 0)
                {
                    Console.WriteLine("");
                    Console.WriteLine($"You managed to pay the entire amount with {numberOfNotesPaid}notes and {numberOfCoinsPaid}coins!");
                }
                else
                {
                    if (restCoin.Value != 0 && restCoin.Value < restNote.Value)
                    {
                        Console.WriteLine($"You have a rest of {amount} to pay with a coin of value {restCoin.Value}");
                        Console.WriteLine("Change expected");
                    }
                    else if (restNote.Value != 0)
                    {
                        Console.WriteLine($"You have a rest of {amount} to pay with a note of value {restNote.Value}");
                        Console.WriteLine("Change expected");
                    }
                }
                updateBalance();
                printBalance();
                return true;
            }
            else
            {
                Console.WriteLine("Not sufficient funds.");
                return false;
            }


            
        }
    }
}
