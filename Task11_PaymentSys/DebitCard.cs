﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSys
{
    class DebitCard : Card, IPayment
    {


        public void Payment()
        {
            bool paymAccept = false;
            int paymen;
            Console.WriteLine("You will now pay with your Debit Card");
            do {
                Console.WriteLine("Please enter amount:");
                paymAccept = int.TryParse(Console.ReadLine(), out paymen);
                if (paymAccept)
                {
                    Console.WriteLine($"You try to withdraw {paymen} from your debet card");
                    if (!CardBalance.Withdrawal(paymen))
                    {
                        Console.WriteLine("Your bank account do not contain enough money");
                        Console.WriteLine($"Remaining funds: {CardBalance.Reserve}");
                    }
                    else
                    {
                        Console.WriteLine("Whithdrawal accepted");
                        Console.WriteLine($"Your account now contains: {CardBalance.Reserve}");
                    }
                }

            } while (!paymAccept);          
        }
    }
}
