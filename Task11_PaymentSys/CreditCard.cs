﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSys
{
    class CreditCard : Card, IPayment
    {
        private int interrest;

        public void Payment()
        {
            bool paymAccept = false;
            int paymen;
            Console.WriteLine("You will now pay with your Credit Card");
            do
            {
                Console.WriteLine("Please enter an amount:");
                paymAccept = int.TryParse(Console.ReadLine(), out paymen);
                if (paymAccept)
                {
                    Console.WriteLine($"You try to withdraw {paymen} from your credit card");
                    if (!CardBalance.Withdrawal(paymen))
                    {
                        Console.WriteLine("You do not have enough credit left on your card.");
                        Console.WriteLine($"Remaining credit: {CardBalance.Reserve}");
                    }
                    else
                    {
                        Console.WriteLine("Whithdrawal accepted");
                        Console.WriteLine($"Remaining credit: {CardBalance.Reserve}");
                    }
                }

            } while (!paymAccept);
        }


        #region unused functions for credit interrest
        public void Interrest()
        {
            Console.WriteLine($"you'r credit cards interrest is {interrest}%");
        }
        public void SetInterrest(int inter)
        {
            interrest = inter;
        }
        public int getInterrest()
        {
            return interrest;
        }
        #endregion

    }
}
