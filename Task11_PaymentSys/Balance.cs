﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSys
{
    class Balance
    {
        private int reserve;

        public Balance()
        {
            Console.WriteLine("Balance added");
            reserve = 0;
        }
        public Balance(int amount)
        {
            reserve = amount;
        }

        public int Reserve { get => reserve; set => reserve = value; }

        /// <summary>
        /// Function for checking if reserve has enough funds for a withdrawal.
        /// If reserve is large enough, function will withdraw the amount form the reserve.
        /// </summary>
        /// <param name="amount"></param>
        /// <returns>true if valid withdrawal</returns>
        public bool Withdrawal(int amount)
        {
            if(amount > reserve)
            {
                return false;
            }
            else
            {
                reserve -= amount;
                return true;
            }
        }

        /// <summary>
        /// Function for depositing an amount into the reserve. 
        /// </summary>
        /// <param name="amount"></param>
        public void Deposit(int amount)
        {
            reserve += amount;
        }
    }
}
